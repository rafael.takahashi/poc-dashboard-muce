import { Card, CardContent, Chip, Typography } from "@mui/material";
import React from "react";
import { PUBLIC_FOLDER } from "../../notview/Constants/Constants";

type CardTransactionProps = {
  userFolder: string; // for getting a profile picture
  userName: string;
  transactionValue: number; // positive or negative
  transactionReason: string;
  transactionPlace: string;
  transactionDate: string;
};

const CardTransaction = (props: CardTransactionProps) => {
  const obteveOuGastou = props.transactionValue >= 0 ? "obteve" : "gastou";

  return (
    <Card sx={{ minWidth: "150px", margin: "10px" }}>
      <CardContent style={{ backgroundColor: "#20b020" }}>
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "flex-start",
            alignItems: "center",
          }}
        >
          <img
            src={`${PUBLIC_FOLDER}/users/${props.userFolder}/profile_picture.png`}
            height={60}
            width={60}
            style={{ objectFit: "cover" }}
            alt={props.userName}
          />
          <div style={{ height: "5px", width: "10px" }} />
          <Typography
            variant="h3"
            sx={{ fontSize: 18, color: "white", fontWeight: "bold" }}
          >
            {`${props.userName} ${obteveOuGastou} 💠${Math.abs(
              props.transactionValue
            )}`}
          </Typography>
        </div>
        <Typography
          sx={{
            fontSize: 16,
            color: "white",
            textAlign: "left",
            margin: "8px 0px 8px 0px",
          }}
          variant="h4"
        >
          {props.transactionReason}
        </Typography>
        <div
          style={{ display: "flex", flexDirection: "row", flexWrap: "wrap" }}
        >
          <Chip
            label={props.transactionPlace}
            size="small"
            style={{
              backgroundColor: "#c0c0c0",
            }}
          />
        </div>

        <Typography sx={{ fontSize: 13, color: "white", textAlign: "right" }}>
          {props.transactionDate}
        </Typography>
      </CardContent>
    </Card>
  );
};

export default CardTransaction;
