import { Typography } from "@mui/material";
import React, { useMemo } from "react";
import { PUBLIC_FOLDER } from "../../notview/Constants/Constants";

type CardAboutProps = {
  folderName: string;
};

const EmptyCard = () => {
  return (
    <div
      style={{
        width: "100%",
        height: 80,
        backgroundColor: "#d0d0d0",
      }}
    >
      <Typography
        style={{
          fontSize: 28,
          fontWeight: "bold",
          color: "white",
          lineHeight: 1.1,
          padding: 10,
        }}
      >
        Clique aqui para ir à sua pasta
      </Typography>
    </div>
  );
};

const getFileName = (folderName: string) => {
  return `${PUBLIC_FOLDER}/users/${folderName}/sobre.png`;
};

export const CardAbout = (props: CardAboutProps) => {
  const filename = useMemo(() => getFileName(props.folderName), []);

  return (
    <a href={filename} target="_blank" rel="noreferrer">
      <div style={{ margin: 10 }}>
        {filename ? (
          <img
            alt="Sobre"
            src={filename}
            width="100%"
            onError={(e) => {
              e.currentTarget.src = `${PUBLIC_FOLDER}/sobre_nao_encontrado.png`;
            }}
          />
        ) : (
          <EmptyCard />
        )}
      </div>
    </a>
  );
};
