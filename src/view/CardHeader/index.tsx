import { Typography } from "@mui/material";
import React from "react";
import { PUBLIC_FOLDER } from "../../notview/Constants/Constants";

type CardCertificatesListProps = {
  name: string;
  folderName: string;
  jobTitle: string;
  currentPosition: string;
  tallyCurrent: number;
  tallyTotal: number;
};

export const CardHeader = (props: CardCertificatesListProps) => {
  return (
    <div style={{ display: "flex", flex: 1, flexDirection: "row" }}>
      {/** TODO: Implement custom pictures. Read them from the user's folder */}
      <img
        src={`${PUBLIC_FOLDER}/users/${props.folderName}/profile_picture.png`}
        height={80}
        width={80}
        style={{ objectFit: "cover" }}
        alt={props.name}
        onError={(e) => {
          e.currentTarget.src = `${PUBLIC_FOLDER}/default_profile_picture.png`;
        }}
      />
      <div
        style={{
          display: "flex",
          flex: 1,
          alignSelf: "center",
        }}
      >
        {/** Fixed padding between picture and the content to the right. */}
        <div
          style={{
            display: "flex",
            flexGrow: 0, // Do not grow
            flexShrink: 0, // Do not shrink
            flexBasis: 10, // Fixed size
          }}
        />
        {/**
         * Div to hold everything to the right of the profile picture.
         * This holds two divs in a vertical stack. They're aligned to the bottom.
         */}
        <div
          style={{
            display: "flex",
            flex: 1,
            flexDirection: "column",
            justifyContent: "flex-end",
          }}
        >
          {/**
           * Div to hold the person's name and the tally, horizontally.
           */}
          <div style={{ display: "flex", flex: 1, flexDirection: "row" }}>
            <div style={{ display: "flex", flex: 1 }}>
              <Typography
                variant="h3"
                sx={{
                  fontSize: 18,
                  color: "white",
                  fontWeight: "bold",
                  textAlign: "left",
                }}
              >
                {props.name}
              </Typography>
            </div>
            <div
              style={{
                display: "flex",
                flexDirection: "column",
                alignItems: "flex-start",
              }}
            >
              <Typography
                variant="h5"
                sx={{ fontSize: 12, color: "white", fontWeight: "bold" }}
              >
                💠{props.tallyCurrent} bits
              </Typography>
              <Typography
                variant="h5"
                sx={{ fontSize: 12, color: "white", fontWeight: "bold" }}
              >
                💠{props.tallyTotal} bits total
              </Typography>
            </div>
          </div>
          {/**
           * Div to hold some text under the person's name.
           */}
          <div
            style={{
              display: "flex",
              flex: 1, // must be able to expand horizontally
              alignSelf: "flex-start",
              flexDirection: "row",
            }}
          >
            <Typography
              variant="h5"
              sx={{ fontSize: 11, color: "white", textAlign: "left" }}
            >
              <span style={{ fontSize: 12, fontWeight: "bold" }}>
                {props.jobTitle}
              </span>
              {" / "}
              {props.currentPosition}
            </Typography>
          </div>
        </div>
      </div>
    </div>
  );
};
