import { Card, CardContent } from "@mui/material";
import React from "react";
import { UserTally } from "../../notview/Types/Types";
import { CardHeader } from "../CardHeader";
import { CardCertificationsList } from "../CardCertificationsList";
import { CardAbout } from "../CardAbout";

type CardProps = {
  user: UserTally;
};

/**
 * Renders a card for a user, containing name, tally, profile picture,
 * certificates, and maybe more.
 */
const UserCard = (props: CardProps) => {
  return (
    <div>
      <Card sx={{ minWidth: 300 }}>
        <CardContent style={{ backgroundColor: "#2020d0" }}>
          <CardHeader
            name={props.user.nome}
            folderName={props.user.pasta}
            tallyCurrent={props.user.bitsAtuais}
            tallyTotal={props.user.bitsHistorico}
            jobTitle={props.user.cargo}
            currentPosition={props.user.atividade}
          />
          <CardCertificationsList certifications={props.user.certificacoes} />
          <CardAbout folderName={props.user.pasta} />
        </CardContent>
      </Card>
    </div>
  );
};

export default UserCard;
