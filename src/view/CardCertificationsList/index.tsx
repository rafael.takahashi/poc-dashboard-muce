import { Typography } from "@mui/material";
import React from "react";
import { Certification } from "../../notview/Types/Types";

type CardCertificationsListProps = {
  certifications: Certification[];
};

type _CardCertificationItemProps = {
  certification: Certification;
};

const categoryColorMap: { [key: string]: string } = {
  java: "#fff2e6",
  python: "#b3d9ff",
  javascript: "#ffffe6",
  idiomas: "#ffb3b3",
  aws: "#e6ffe6",
};

const CardCertificationItem = (props: _CardCertificationItemProps) => {
  const textTopLeft = props.certification.tipo;
  const textCenter = `${props.certification.entidade} ${props.certification.nome}`;
  const textBottomLeft = props.certification.codigo;
  const textBottomRight = props.certification.validade
    ? `${props.certification.emissao} (val.: ${props.certification.validade})`
    : props.certification.emissao;

  const backgroundColor =
    categoryColorMap[props?.certification?.categoria?.toLocaleLowerCase()] ??
    "#d8d8d8";

  return (
    <a
      href={props.certification.link ?? ""}
      target="_blank"
      style={{ textDecoration: "none" }}
      rel="noreferrer"
    >
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          justifyContent: "space-between",
          alignItems: "stretch",
          backgroundColor: backgroundColor,
          padding: 3,
        }}
      >
        {/** First line */}
        <div style={{ display: "flex", justifyContent: "space-between" }}>
          <Typography sx={{ fontSize: 10, color: "black" }}>
            {textTopLeft}
          </Typography>
          <div />
        </div>
        {/** Second line */}
        <div
          style={{
            display: "flex",
            justifyContent: "space-between",
            alignSelf: "stretch",
            marginTop: 2,
            marginBottom: 2,
          }}
        >
          <Typography
            sx={{
              fontSize: 14,
              fontWeight: "bold",
              color: "black",
              textAlign: "left",
              lineHeight: 1,
            }}
          >
            {textCenter}
          </Typography>
        </div>
        {/** Third line */}
        <div style={{ display: "flex", justifyContent: "space-between" }}>
          <Typography sx={{ fontSize: 10, color: "black" }}>
            {textBottomLeft}
          </Typography>
          <Typography sx={{ fontSize: 10, color: "black" }}>
            {textBottomRight}
          </Typography>
        </div>
      </div>
    </a>
  );
};

export const CardCertificationsList = (props: CardCertificationsListProps) => {
  return (
    <div
      style={{
        display: "flex",
        flex: 1,
        flexDirection: "row",
        alignItems: "flex-start",
        flexWrap: "wrap",
      }}
    >
      {props.certifications.map((cert) => (
        <div
          style={{
            width: "48%",
            margin: "1%",
            minHeight: 50,
            backgroundColor: "orange",
          }}
        >
          <CardCertificationItem certification={cert} />
        </div>
      ))}
    </div>
  );
};
