import { TextField, Typography } from "@mui/material";
import React, { useEffect, useMemo, useState } from "react";
import "./App.css";
import { Transaction, UserTally } from "./notview/Types/Types";
import UserCard from "./view/UserCard";
import { loadTransactions, loadUserTally } from "./notview/Parser/Parser";
import CardTransaction from "./view/CardTransaction";

function App() {
  const [transactions, setTransactions] = useState<Transaction[]>([]);
  const [userTally, setUserTally] = useState<UserTally[]>([]);

  useEffect(() => {
    loadTransactions().then((transactions) => {
      const reversedTransactions = transactions.slice(0);
      reversedTransactions.reverse(); // Show newest at the top
      setTransactions(reversedTransactions);
      loadUserTally(transactions).then((userTally) => {
        // Show users with more bits first.
        const reorderedTally = userTally.slice(0);
        reorderedTally.sort((a, b) =>
          a.bitsHistorico < b.bitsHistorico ? 1 : -1
        );
        setUserTally(reorderedTally);
      });
    });
  }, []);

  const debugTextFieldContent = useMemo(() => {
    return `DEBUG\n\n\ntransactions: ${JSON.stringify(
      transactions,
      null,
      2
    )}\n\n\ntally ${JSON.stringify(userTally, null, 2)}`;
  }, [transactions, userTally]);

  return (
    <div className="App">
      <header>
        <Typography variant="h1">POC Dashboard dos MUCEs</Typography>
      </header>
      <body>
        <p>TODOS OS DADOS SÃO FICTÍCIOS</p>
        <p>
          Leia o readme {`=>`}{" "}
          <a
            href="https://gitlab.com/rafael.takahashi/poc-dashboard-muce"
            target="_blank"
            rel="noreferrer"
          >
            https://gitlab.com/rafael.takahashi/poc-dashboard-muce
          </a>
        </p>
        <p>(use o link postado no nosso Teams para editar o repositório)</p>
        <p>
          Não coloque dados importantes aqui. Este link é público, e o
          repositório também.
        </p>
        <div
          style={{
            display: "flex",
            flex: 1,
            flexDirection: "row",
            alignItems: "flex-start",
          }}
        >
          <div
            style={{
              display: "flex",
              flex: 1,
              flexDirection: "row",
              flexWrap: "wrap",
            }}
          >
            {userTally.map((user) => (
              <div style={{ margin: 10, maxWidth: 400 }}>
                <UserCard user={user} />
              </div>
            ))}
          </div>

          <div
            style={{
              display: "flex",
              flexGrow: 0, // do not grow
              flexShrink: 0, // do not shrink
              flexBasis: "320px", // fixed size
              flexDirection: "column",
              width: 120,
            }}
          >
            <Typography variant="h3" sx={{ fontSize: 16, fontWeight: "bold" }}>
              💠 Transações recentes na Muralis 💠
            </Typography>
            {transactions.map((transaction) => {
              const userInfo = userTally.filter(
                (user) => user.email === transaction.recebedor
              )[0];
              if (!userInfo) return null;
              return (
                <CardTransaction
                  userFolder={userInfo.pasta}
                  userName={userInfo.nome}
                  transactionValue={transaction.bits}
                  transactionReason={transaction.motivo}
                  transactionPlace={transaction.local}
                  transactionDate={transaction.data}
                />
              );
            })}
          </div>
        </div>

        <TextField disabled multiline fullWidth value={debugTextFieldContent} />
      </body>
    </div>
  );
}

export default App;
