import Papaparse from "papaparse";
import { PUBLIC_FOLDER } from "../Constants/Constants";
import { Certification, Transaction, User, UserTally } from "../Types/Types";

export function loadTransactions(): Promise<Transaction[]> {
  return new Promise((resolve, reject) => {
    console.log("Parsing transactions...");
    Papaparse.parse(PUBLIC_FOLDER + "/tally.csv", {
      delimiter: ",",
      header: true,
      dynamicTyping: true,
      download: true,
      complete: (results: any, file: any) => {
        console.log(
          `Success parsing transactions. ${results.data.length} transactions read.`
        );
        console.log(results.data);
        console.log(results.errors);
        console.log(file);
        resolve(results.data as any as Transaction[]);
      },
      error: (error: any) => {
        console.log("Error parsing transactions.");
        console.log(error);
        reject(error);
      },
    });
  });
}

function _loadCertifications(folderName: string): Promise<Certification[]> {
  return new Promise((resolve, reject) => {
    console.log(`Parsing certifications in folder ${folderName}...`);
    Papaparse.parse(`${PUBLIC_FOLDER}/users/${folderName}/cert.csv`, {
      delimiter: ",",
      header: true,
      dynamicTyping: true,
      download: true,
      complete: (results: any, file: any) => {
        console.log(`Success parsing certifications in folder ${folderName}`);
        console.log(results.data);
        resolve(results.data as any as Certification[]);
      },
      error: (error: any) => {
        console.log(`Error parsing certifications in folder ${folderName}`);
        reject(error);
      },
    });
  });
}

async function _loadUsers(): Promise<User[]> {
  // For each user, we must also read the list of certifications inside that
  // user's folder.
  // We must first read the user information from users.csv because the folder
  // names for each user are also written in there.
  // Then, we can read all the csv files individually, yielding an empty
  // array for any error (since the csv could simply not exist). These
  // individual reads can happen in parallel.
  const promiseToReadCsv = new Promise<User[]>((resolve, reject) => {
    console.log(`Parsing users...`);
    Papaparse.parse(PUBLIC_FOLDER + "/users/users.csv", {
      delimiter: ",",
      header: true,
      dynamicTyping: true,
      download: true,
      complete: (results: any, file: any) => {
        console.log(`Success parsing users.`);
        console.log(results.data);
        resolve(results.data as any as User[]);
      },
      error: (error: any) => {
        console.log(`Error parsing users.`);
        reject(error);
      },
    });
  });

  const usersWithoutCert = await promiseToReadCsv;

  // Read each cert csv individually and in parallel.
  const promiseToReadEachCertCsv = usersWithoutCert.map((u) =>
    _loadCertifications(u.pasta)
  );

  // The catch makes all errors silent. Not great, but errors are printed.
  const eachCertCsv = await Promise.all(
    promiseToReadEachCertCsv.map((promise) =>
      promise.catch((reason) => [] as Certification[])
    )
  );

  // Combine each user with their list of certifications to make a new list.
  const usersWithCert = [] as User[];
  for (var i = 0; i < usersWithoutCert.length; i++) {
    usersWithCert.push({
      ...usersWithoutCert[i],
      certificacoes: eachCertCsv[i],
    });
  }

  return usersWithCert;
}

export async function loadUserTally(
  transactions: Transaction[]
): Promise<UserTally[]> {
  try {
    const users = await _loadUsers();
    if (!transactions) {
      transactions = await loadTransactions();
    }

    return users.map<UserTally>((user) => {
      var grandTotal = 0;
      var available = 0;
      for (var i = 0; i < transactions.length; i++) {
        if (transactions[i].recebedor === user.email) {
          const transactionValue = transactions[i].bits;
          available += transactionValue;
          if (transactionValue > 0) {
            grandTotal += transactionValue;
          }
        }
      }
      return {
        ...user,
        bitsAtuais: available,
        bitsHistorico: grandTotal,
      };
    });
  } catch (e) {
    return [];
  }
}
