export type Transaction = {
  responsavel: string; // email address
  recebedor: string; // email address
  bits: number; // integer number of bits
  data: string;
  local: string;
  motivo: string;
};

export type Certification = {
  tipo: string; // "certificado, "treinamento", etc.
  entidade: string;
  nome: string;
  codigo?: string; // código do curso/treinamento/certificação, se houver
  categoria: string; // "java", "python", etc.
  emissao: string; // "Junho 2020", "2021", "20 de Setembro de 2016", etc.
  validade?: string; // Mesmo que a data
  link: string;
};

export type User = {
  email: string; // email address
  nome: string; // full name
  cargo: string; // "Dev", "QA", "Engenheiro", etc.
  atividade: string; // "Dev java na ACB", "QA no projeto DEF", etc
  pasta: string; // folder name
  certificacoes: Certification[];
};

export type UserTally = User & {
  bitsAtuais: number; // bits available to spend
  bitsHistorico: number; // grand total
};
