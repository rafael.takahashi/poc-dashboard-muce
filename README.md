# POC Dashboard dos MUCEs

Bem vind@s ao POC dos dashboards dos MUCEs! Sinta-se livre para experimentar e dar feedback. A ideia deste POC é pensar em como podemos montar um dashboard mais definitivo para encorajar a participação nos MUCEs, treinamentos e certificações.

## ⭐️ 👉 Para Criar seu Card 👈 ⭐️

Tudo neste POC ainda é bastante manual, e tudo é lido de arquivos locais dentro do próprio repositório. Isso significa que para adicionar um card para você, é necessário commitar suas mudanças no repositório. (Não temos branches protegidas, pode commitar na main, só veja se não quebrou o projeto rodando um "yarn start")

O repositório é público, mas para poder editar, use o link postado no nosso Teams, que possui um _deploy token_ incluído na url.

_Não inclua dados importantes. O link do POC é público e o repositório também._

Primeiro, edite o arquivo `public/users/users.csv`; adicione uma linha para você. Pode escolher o nome da pasta. (Não deixe linhas vazias nos arquivos csv e não use espaços depois de vírgulas, por favor!)

Depois, adicione uma pasta nova em `public/users` usando o nome de pasta que você escolheu. Pode usar as outras pastas como exemplo; você precisa de um arquivo `cert.csv` e um `sobre.png` dentro da sua pasta.

- O `cert.csv` vai conter suas certificações e treinamentos. Cada linha renderiza como um card colorido dentro do seu próprio card. Os campos são os seguintes:
  - "tipo" pode ser qualquer string. Nos exemplos, tem "Certificação" e "Treinamento".
  - "entidade" é a entidade certificadora. Aparece no título do card.
  - "nome" é o nome da certificação. Aparece no título do card.
  - "código" é opcional, se for relevante.
  - "categoria" é o tipo da certificação; a cor do card muda de acordo com a categoria. Pode editar o arquivo `src/view/CardCertificationsList/index.tsx` se quiser adicionar alguma cor.
  - "emissao" é a data de emissão, pode ser qualquer string.
  - "validade" é opcional, se houver uma data de validade. Também pode ser qualquer string.
  - "link" é o link que vai ser aberto se você clicar no card. Você pode usar para direcionar para a imagem do seu certificado.
- O `sobre.png` pode ser qualquer imagem. Existe uma planilha em `public/about_template.xlsx` para usar de exemplo, é só preencher localmente e tirar uma screenshot. Mas você também pode usar qualquer imagem que quiser.
- `profile_picture.png` é uma foto sua, se quiser usar. Se não, vai aparecer uma imagem padrão. A imagem será cortada para caber em um quadrado.

**TODOS OS DADOS SÃO FICTÍCIOS**. Este projeto é um ponto de partida para planejarmos um dashboard de verdade.

## Bits

Uma ideia que temos é introduzir uma espécie de moeda para usarmos internamente na Muralis, que pode ser trocada por itens numa loja. Esta moeda não estará atrelada a dinheiro real (não podendo ser comprada ou vendida por Reais).

Este POC inclui um arquivo `public/tally.csv` onde você pode atribuir quantidades de "bits" para pessoas diferentes, e essa quantidade vai refletir nos "bits" mostrados no card de cada pessoa. O card mostra a quantidade atual de bits disponíveis e a quantidade ganha até o momento, como uma forma de representar progresso.

Quando implementarmos esse sistema de verdade, cada pessoa vai poder obter bits por participação nos MUCEs, por exemplo ao tirar certificações, ao fazer treinamentos, ao estar presente em reuniões, etc.

## Desenvolvimento Futuro

Este projeto é nada além de um POC. O plano é vários MUCEs se juntarem para montar um dashboard mais definitivo (que não faça as pessoas editarem arquivos csv!) para ser usado por todos que quiserem dentro da Muralis.
